<?php
	class Tables {

		public function show_data(){
			//load datatables library
			$this->load->library('datatables');
			$data=$this->datatables->select('data_id as id, data_code as code, data_name as name')
			->from('data')
			->where('data_filter','Y');

			echo $this->datatables->generate('json', '');
		}

	}
?>